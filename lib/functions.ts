import * as rl from "readline-sync";

export function cin(text: string, args?: any) {
    return (empty(args)) ? rl.question(`${text}`) : rl.question(`${text}`, args);
}

export function cout(data: any) {
    console.log(data);
    return;
}

export function empty(val) {
    if (val === undefined)
        return true;
    if (typeof (val) == 'function' || typeof (val) == 'number' || typeof (val) == 'boolean' || Object.prototype.toString.call(val) === '[object Date]')
        return false;
    if (val == null || val.length === 0)
        return true;
    if (typeof (val) == "object") {
        var r = true;
        for (var f in val)
            r = false;
        return r;
    }
    return false;
}