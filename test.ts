import { cout, auth } from './lib';

async function startJob() {
    cout("---------------")
    await auth();
}

startJob().then((r) => {
    cout("---------------")
}).catch((err) => {
    cout("---------------")
})