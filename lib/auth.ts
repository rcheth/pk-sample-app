import * as assert from "assert";
import { SessionChallenge } from "pk-client";
import { cin, cout } from "./functions";
export async function auth() {
  let base_url =
    cin("Enter Base URL (enter to skip) :") ||
    "https://api-stage.paysack.com/demo";
  let username = cin("Enter username (registered email) :") || "";
  assert.ok(username, "Invalid UserName");
  let password = cin(`Please enter password:`, { hideEchoBack: true });
  await new SessionChallenge(base_url, username, password)
    .login()
    .then((r) => {
      assert.ok(r.token, r);
      cout("===============");
      cout(r.token);
      cout("===============");
    })
    .catch((e) => {
      cout("==== ERROR ====");
      cout(e.response.data.error.includes("_id") ? "Invalid Username" : e.response.data.error);
      cout("==== ERROR ====");
    });
  return;
}
