# **PK-SAMPLE-APP**

## Pre-requisites

1. Install NodeJS
2. Install NPM

## Download

- Using GIT or,
- Using web via ZIP

After extracting, navigate in terminal/cmd to `pk-sample-app`

then run commands as below;

## Install Dependencies

`npm install`

## Run Application

`npm start`
